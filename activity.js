// Get method

fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => console.log(json))

// title only
fetch("https://jsonplaceholder.typicode.com/todos")
.then(response => response.json())
.then(json => {
    const title = json.map(todo => todo.title);
    console.log(title);
})


// title and status
fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(response => response.json())
.then(json => console.log(`The item ${json.title} on the list has a status of ${json.completed}`))


// POST
fetch("https://jsonplaceholder.typicode.com/todos", {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    completed: "False",
    title: "Created To Do List Item",
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log(json))


//PUT
fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: 'PUT',
  headers: {
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({
    title: "Update To Do List Item",
    description: "To update to my list with a different data structure",
    status: "Pending",
    dateCompleted: "Pending",
    userId: 1
  })
})
.then(response => response.json())
.then(json => console.log(json))


// PATCH
fetch('https://jsonplaceholder.typicode.com/todos/1', {
method: 'PATCH',
headers: {
    'Content-Type': 'application/json'
  },
body: JSON.stringify({
    dateCompleted: "Pending",
  })
})
.then(response => response.json())
.then(json => console.log(json))


// DELETE
fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "DELETE"
})
